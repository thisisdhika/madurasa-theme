const plugin = require('tailwindcss/plugin')

module.exports = {
  purge: false,
  theme: {
    container: {
      center: true,
      padding: {
        default: '1rem',
        mobile: '2rem',
        tablet: '4rem',
        laptop: '4.5rem',
        desktop: '5rem',
        hddesktop: '6rem'
      }
    },
    maxHeight: {
      '0': '0',
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
      '3/4': '75%',
      'contact-left': '85%',
      'img-block': '125%',
      full: '100%'
    },
    minHeight: {
      '400': '400px',
      '500': '500px',
      '600': '600px',
      '50vh': '50vh',
      screen: '100vh'
    },
    inset: {
      'product-img': '-8rem',
      honeycomb: '-2.5rem',
      'honeycomb-mobile': '-0.5rem',
      auto: 'auto',
      0: '0'
    },
    extend: {
      width: {
        'articles-mobile': '220vw'
      },
      height: {
        'max-content': 'max-content'
      },
      screens: {
        mobile: '640px',
        tablet: '768px',
        laptop: '1024px',
        desktop: '1280px',
        hddesktop: '1440px'
      },
      colors: {
        white: '#FFF',
        orange: {
          default: '#F7941C',
          '100': '#f6f2ec',
          '200': '#FEF0DB',
          '400': '#dc8822'
        },
        gray: {
          '300': '#bdbdbd',
          '400': '#EFEFEF',
          '900': '#5C564E'
        }
      }
    }
  },
  variants: {},
  plugins: [
    require('@tailwindcss/typography'),
    plugin(function({ addBase, config }) {
      addBase({
        h1: {
          fontSize: config('theme.fontSize.4xl'),
          lineHieght: config('theme.lineHeight.tight'),
          marginBottom: config('theme.spacing.4')
        },
        h2: {
          fontSize: config('theme.fontSize.3xl'),
          lineHieght: config('theme.lineHeight.tight'),
          marginBottom: config('theme.spacing.3')
        },
        h3: {
          fontSize: config('theme.fontSize.2xl'),
          lineHieght: config('theme.lineHeight.tight'),
          marginBottom: config('theme.spacing.2')
        },
        p: { display: 'block', marginBottom: config('theme.spacing.2'), lineHeight: 1.7 }
      })
    })
  ],
  corePlugins: {
    preflight: false
  }
}
