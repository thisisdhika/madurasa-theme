<?php

if (!defined('ABSPATH'))
    die('-1');

class contact_info_widget extends WP_Widget
{

    /** constructor */
    function __construct()
    {
        parent::__construct(
            'contact_info_widget',
            __('Contact Info', 'madurasa'),
            array('description' => __('Display your contact info', 'madurasa'),)
        );
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance)
    {
        extract($args);
        $title  = apply_filters('widget_title', $instance['title']);
        $email  = $instance['email'];
        $phone  = $instance['phone'];

        echo $before_widget;
        if ($title)
            echo $before_title . $title . $after_title; ?>

        <?php
        if (!empty($email))
            echo '<a href="mailto:' . $email . '" class="mr-4"><i class="fas fa-envelope fa-lg mr-2"></i>' . $email . '</a>';
        if (!empty($phone))
            echo '<a href="tel:' . $phone . '"><i class="fas fa-phone fa-lg mr-2"></i>' . $phone . '</a>';
        ?>

        <?php echo $after_widget; ?>
    <?php
    }

    /** @see WP_Widget::update  */
    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['email'] = strip_tags($new_instance['email']);
        $instance['phone'] = strip_tags($new_instance['phone']);
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance)
    {

        $title  = esc_attr($instance['title']);
        // $about  = esc_attr($instance['about']);
        $email  = esc_attr($instance['email']);
        $phone  = esc_attr($instance['phone']);

    ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email:', 'simple-contact-info-widget'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo $email; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone:', 'simple-contact-info-widget'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo $phone; ?>" />
        </p>
<?php
    }
} // end class

function register_contact_info_widget()
{
    return register_widget("contact_info_widget");
}

add_action('widgets_init', "register_contact_info_widget");
