<?php

/**
 * 
 * Template Name: Honey Wellness Template
 * Template Post Type: page
 *
 */


$context = Timber::context();

$timber_post     = new Timber\Post();
$context['post'] = $timber_post;
$context['terms'] = Timber::get_terms('article_type');

Timber::render(array('templates/honey-wellness.twig', 'page.twig'), $context);
