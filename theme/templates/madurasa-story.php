<?php

/**
 * 
 * Template Name: Madurasa Story Template
 * Template Post Type: page
 *
 */


$context = Timber::context();

$timber_post     = new Timber\Post();
$context['post'] = $timber_post;

Timber::render(array('templates/madurasa-story.twig', 'page.twig'), $context);
