<?php

/**
 * 
 * Template Name: Landing Template
 * Template Post Type: page
 *
 */


$context = Timber::context();

$timber_post            = new Timber\Post();
$context['post']        = $timber_post;

$context['honey_ideas'] = Timber::get_posts(array(
    'post_type' => 'honey_ideas',
    "post_status"    => "publish",
    "posts_per_page" => 3,
    'ignore_sticky_posts' => 1
));

$context['honey_and_wellness'] = Timber::get_posts(array(
    'post_type' => 'post',
    "post_status"    => "publish",
    "posts_per_page" => 3,
    'ignore_sticky_posts' => 1
));

Timber::render(array('templates/landing.twig', 'page.twig'), $context);
