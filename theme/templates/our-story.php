<?php

/**
 * 
 * Template Name: Our Story Template
 * Template Post Type: page
 *
 */


$context = Timber::context();

$timber_post     = new Timber\Post();
$context['post'] = $timber_post;

Timber::render(array('templates/our-story.twig', 'page.twig'), $context);
