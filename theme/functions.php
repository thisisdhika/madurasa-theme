<?php

/**
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

/**
 * If you are installing Timber as a Composer dependency in your theme, you'll need this block
 * to load your dependencies and initialize Timber. If you are using Timber via the WordPress.org
 * plug-in, you can safely delete this block.
 */
$composer_autoload = dirname(__DIR__) . '/vendor/autoload.php';

use Carbon\Carbon;

if (file_exists($composer_autoload)) {
	require_once $composer_autoload;

	$timber = new Timber\Timber();
}

/**
 * This ensures that Timber is loaded and available as a PHP class.
 * If not, it gives an error message to help direct developers on where to activate
 */
if (!class_exists('Timber')) {

	add_action(
		'admin_notices',
		function () {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
		}
	);

	add_filter(
		'template_include',
		function ($template) {
			return dirname(get_stylesheet_directory()) . '/static/no-timber.html';
		}
	);
	return;
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array('../views');

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;

require_once get_template_directory() . '/includes/functions.php';
require_once get_template_directory() . '/includes/theme-preferences.php';
require_once get_template_directory() . '/widgets/marketplace-widget.php';
require_once get_template_directory() . '/widgets/contact-info-widget.php';
require_once get_template_directory() . '/includes/filters.php';
require_once get_template_directory() . '/includes/scripts.php';

add_action('widgets_init', function () {
	// Arguments used in all register_sidebar() calls.
	$shared_args = array(
		'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
		'after_title'   => '</h2>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget'  => '</div></div>',
	);

	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __('Marketplaces Footer', 'madurasa'),
				'id'          => 'default-footer',
				'description' => "",
			)
		)
	);

	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __('Info Footer', 'madurasa'),
				'id'          => 'info-footer',
				'description' => "",
			)
		)
	);
});

function remove_lang_from_url($url)
{
	$result = $url;
	foreach (get_option('wpm_languages') as $code => $lang) {
		$result = str_replace('/' . $code, '', $result);
	}

	return $result;
}

add_action('template_redirect', function () {
	if (
		isset($_SERVER['HTTPS']) &&
		($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
		isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
		$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'
	) {
		$protocol = 'https://';
	} else {
		$protocol = 'http://';
	}

	$currenturl = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$localedurl = remove_lang_from_url($currenturl);
	$currenturl_relative = wp_make_link_relative($localedurl);

	// switch ($currenturl_relative) {
	// 		case '[from slug]':
	// 			$urlto = home_url('[to slug]');
	// 			break;

	// 	default:
	// 		return;
	// }

	$urlto = '';

	if ($currenturl_relative == remove_lang_from_url(wp_make_link_relative(get_post_type_archive_link('products')))) {
		$urlto = Timber::get_terms('product_categories')[0]->link;
	} elseif ($currenturl_relative == remove_lang_from_url(wp_make_link_relative(get_post_type_archive_link('honey_faq')))) {
		$urlto = Timber::get_terms('faq_categories')[0]->link;
	} else {
		return;
	}

	if ($currenturl != $urlto)
		exit(wp_redirect($urlto));
});

/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class MadurasaSite extends Timber\Site
{
	/** Add timber support. */
	public function __construct()
	{
		add_action('after_setup_theme', array($this, 'theme_supports'));
		add_filter('timber/context', array($this, 'add_to_context'));
		add_filter('timber/twig', array($this, 'add_to_twig'));
		add_action('init', array($this, 'setup'));
		add_action('init', array($this, 'register_post_types'));
		add_action('init', array($this, 'register_taxonomies'));

		parent::__construct();
	}
	/** This is where you can register custom post types. */
	public function register_post_types()
	{
		require_once get_template_directory() . '/includes/post-types.php';
	}
	/** This is where you can register custom taxonomies. */
	public function register_taxonomies()
	{
		unregister_taxonomy_for_object_type('post_tag', 'post');

		require_once get_template_directory() . '/includes/taxonomies.php';
	}

	public function setup()
	{
		//
	}

	/** This is where you add some context
	 *
	 * @param string $context context['this'] Being the Twig's {{ this }}.
	 */
	public function add_to_context($context)
	{
		$context['custom_logo'] = get_custom_logo();
		$context['default_footer'] = is_active_sidebar('default-footer') ? Timber::get_widgets('default-footer') : false;
		$context['info_footer'] = is_active_sidebar('info-footer') ?  Timber::get_widgets('info-footer') : false;
		$context['header_menu']  = new Timber\Menu('header-menu');
		$context['social_menu']  = new Timber\Menu('social-menu');
		$context['shared_counts']  = do_shortcode('[shared_counts]');
		$context['cur_post_type']  = new Timber\PostType(get_post_type());
		$context['cur_lang']  = explode('_', get_locale())[0];

		// Theme Config
		$context['theme_config']  = array(
			"post_button_label" => get_option("_mdrs_post_button_label"),
			"products_button_label" => get_option("_mdrs_products_button_label"),
			"honey_ideas_button_label" => get_option("_mdrs_honey_ideas_button_label"),
			"news_and_events_button_label" => get_option("_mdrs_news_and_events_button_label"),
			"learn_more_link_label" => get_option("_mdrs_learn_more_link_label"),
			"products_archive_title" => get_option("_mdrs_products_archive_title"),
			"honey_ideas_ttl_home" => get_option("_mdrs_honey_ideas_ttl_home"),
			"honey_ideas_description" => get_option("_mdrs_honey_ideas_description"),
			"footer_marketplaces_title" => get_option("_mdrs_footer_marketplaces_title"),
			"footer_certificates_description" => get_option("_mdrs_footer_certificates_description"),
			"footer_socials_label" => get_option("_mdrs_footer_socials_label"),
			"footer_contact_info_email" => get_option("_footer_contact_info_email"),
			"footer_contact_info_phone" => get_option("_footer_contact_info_phone")
		);

		$context['products'] = Timber::get_posts(array(
			'post_type' => 'products',
			"post_status"    => "publish",
			"posts_per_page" => 3,
			'ignore_sticky_posts' => 1
		));

		$context['banner'] = bm_get_current_banner();

		$context['site']  = $this;

		return $context;
	}

	public function theme_supports()
	{
		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		register_nav_menus(
			array(
				'header-menu'   => 'Header Menu',
				'social-menu'	=> 'Socials Menu',
			)
		);

		add_theme_support('menus');

		// Custom logo.
		$logo_width  = 120;
		$logo_height = 90;

		// If the retina setting is active, double the recommended width and height.
		if (get_theme_mod('retina_logo', false)) {
			$logo_width  = floor($logo_width * 2);
			$logo_height = floor($logo_height * 2);
		}

		add_theme_support(
			'custom-logo',
			array(
				'height'      => $logo_height,
				'width'       => $logo_width,
				'flex-height' => true,
				'flex-width'  => true,
			)
		);
	}

	public function get_oembed_url($value)
	{
		preg_match('/href="(.+?)"/', $value, $result);

		return $result[1];
	}

	public function parse_colorable($value)
	{
		preg_match('/\[=(.+?)\]/', $value, $parsed);

		if (count($parsed) > 0) {
			$orig = $parsed;
			$target = explode(' ', $parsed[1]);
			$color = $target[0];
			unset($target[0]);

			$text = implode(" ", $target);
			$result = "<span class='text-{$color}'>{$text}</span>";

			$value = str_replace($orig[0], $result, $value);
		}

		return $value;
	}

	public function get_post_from_url($value)
	{
		$slug = str_replace('/', '', wp_make_link_relative(remove_lang_from_url($value)));

		if ($query = get_page_by_path($slug)) {
			return wpm_translate_object($query);
		} else {
			foreach (get_post_types_by_support('post-formats') as $pt) {
				if (get_post_type_object($pt)->has_archive == $slug) {
					$query = get_post_type_object($pt);

					return $query;
				}
			}
		}

		return $value;
	}

	public function get_banner($query)
	{
		$banner = bm_get_current_banner($query);

		return $banner;
	}

	public function time_diff($datetime)
	{
		return Carbon::createFromDate($datetime)->diffForHumans();
	}

	public function count_rating_stars($posts, $stars)
	{
		$result = 0;

		foreach ($posts as $post) {
			if ($post->_stars_rating == $stars) {
				$result++;
			}
		}

		return $result;
	}

	/** This is where you can add your own functions to twig.
	 *
	 * @param string $twig get extension.
	 */
	public function add_to_twig(object $twig)
	{
		$twig->addExtension(new Twig\Extension\StringLoaderExtension());
		// $twig->addExtension(new \Jasny\Twig\DateExtension());
		// $twig->addExtension(new \Jasny\Twig\PcreExtension());
		// $twig->addExtension(new \Jasny\Twig\TextExtension());
		// $twig->addExtension(new \Jasny\Twig\ArrayExtension());
		$twig->addFilter(new Twig\TwigFilter('get_oembed_url', array($this, 'get_oembed_url')));
		$twig->addFilter(new Twig\TwigFilter('parse_colorable', array($this, 'parse_colorable')));
		$twig->addFilter(new Twig\TwigFilter('get_post_from_url', array($this, 'get_post_from_url')));
		$twig->addFilter(new Twig\TwigFilter('get_banner', array($this, 'get_banner')));
		$twig->addFilter(new Twig\TwigFilter('time_diff', array($this, 'time_diff')));
		$twig->addFilter(new Twig\TwigFilter('count_rating_stars', array($this, 'count_rating_stars')));

		return $twig;
	}
}

new MadurasaSite();
