<?php

/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */
$templates = array('archive/default.twig', 'index.twig');

$context = Timber::context();

$context['title'] = get_post(get_option("page_for_posts")) ? get_post(get_option("page_for_posts"))->post_title : 'Archive';

if (is_day()) {
	$context['title'] = 'Archive: ' . get_the_date('D M Y');
} elseif (is_month()) {
	$context['title'] = 'Archive: ' . get_the_date('M Y');
} elseif (is_year()) {
	$context['title'] = 'Archive: ' . get_the_date('Y');
} elseif (is_tag()) {
	$context['title'] = single_tag_title('', false);
} elseif (is_category()) {
	$context['title'] = single_cat_title('', false);
	array_unshift($templates, 'archive/' . get_query_var('cat') . '.twig');
} elseif (is_tax()) {
	$tax = get_queried_object();

	$context['title'] = $tax->name;
	$context['current_tax'] = $tax;

	if ($tax->taxonomy === 'product_categories') {
		$context['terms'] = Timber::get_terms('product_categories');
	} else if ($tax->taxonomy === 'faq_categories') {
		$context['terms'] = Timber::get_terms('faq_categories');
	} else if ($tax->taxonomy === 'article_type') {
		$context['terms'] = Timber::get_terms('article_type');
	}

	array_unshift($templates, 'archive/' . $tax->taxonomy . '.twig');
} elseif (is_post_type_archive()) {
	$context['title'] = post_type_archive_title('', false);

	array_unshift($templates, 'archive/' . get_post_type() . '.twig');
}

if ($context['cur_post_type'] and isset($context['cur_post_type']->name)) {
	switch ($context['cur_post_type']->name) {
		case 'honey_faq':
			$args = [
				"post_type"      => "honey_faq",
				"post_status"    => "publish",
				"posts_per_page" => -1
			];

			if (isset(get_queried_object()->taxonomy)) {
				$args['tax_query'] = [
					[
						'taxonomy' => get_queried_object()->taxonomy,
						'field' => 'slug',
						'terms' => get_queried_object()->slug
					]
				];
			}

			$context['posts'] = new Timber\PostQuery($args);
			break;

		case 'honey_ideas':
			$context['description'] = get_the_post_type_description();
			$context['posts'] = new Timber\PostQuery();
			break;

		default:
			$context['posts'] = new Timber\PostQuery();
			break;
	}
} else {
	$context['posts'] = new Timber\PostQuery();
}


Timber::render($templates, $context);
