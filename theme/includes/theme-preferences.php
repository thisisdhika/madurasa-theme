<?php

function theme_option_page()
{
?>
    <div class="wrap">
        <h1>Madurasa Theme Preferences</h1>
        <form method="post" action="options.php">
            <?php

            settings_fields("theme-options-grp");

            do_settings_sections("theme-options");

            submit_button();
            ?>
        </form>
    </div>
    <?php
}

function add_theme_menu_item()
{
    add_theme_page("Theme Preferences", "Theme Preferences", "manage_options", "theme-options", "theme_option_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");

function theme_preferences()
{
    add_settings_section(
        'first_section',
        'Button / Link Labels',
        function () {
            echo '<p>Theme Labellings Option</p>';
        },
        'theme-options'
    );

    $post_types = get_post_types_by_support("post-formats");

    foreach ($post_types as $pt) {
        $post_type = get_post_type_object($pt);

        add_settings_field($post_type->name . '_btn_label',  $post_type->label . " Button Label", function () use ($post_type) {
            $label = '_mdrs_' . $post_type->name . '_button_label';
    ?>
            <input type="text" style="width: 400px;" name="<?php echo $label ?>" id="<?php echo $label ?>" value="<?php echo get_option($label); ?>" />
        <?php
        }, 'theme-options', 'first_section');

        register_setting('theme-options-grp', '_mdrs_' . $post_type->name . '_button_label');
    }

    add_settings_field('_learn_more_link_label',  "Learn More Link's Label", function () {
        ?>
        <input type="text" style="width: 400px;" name="_mdrs_learn_more_link_label" placeholder="Ex: 'Learn More', 'Selengkapnya'" id="_mdrs_learn_more_link_label" value="<?php echo get_option('_mdrs_learn_more_link_label'); ?>" />
    <?php
    }, 'theme-options', 'first_section');
    register_setting('theme-options-grp', '_mdrs_learn_more_link_label');

    add_settings_field('_products_archive_title',  "Products Archive Title", function () {
    ?>
        <input type="text" style="width: 400px;" name="_mdrs_products_archive_title" id="_mdrs_products_archive_title" value="<?php echo get_option('_mdrs_products_archive_title'); ?>" />
    <?php
    }, 'theme-options', 'first_section');
    register_setting('theme-options-grp', '_mdrs_products_archive_title');

    add_settings_field('_honey_ideas_ttl_home',  "Section's Honey Ideas Title at Homepage", function () {
    ?>
        <input type="text" style="width: 400px;" name="_mdrs_honey_ideas_ttl_home" id="_mdrs_honey_ideas_ttl_home" value="<?php echo get_option('_mdrs_honey_ideas_ttl_home'); ?>" />
    <?php
    }, 'theme-options', 'first_section');
    register_setting('theme-options-grp', '_mdrs_honey_ideas_ttl_home');

    add_settings_field('_honey_ideas_description',  "Honey Ideas Description", function () {
    ?>
        <textarea style="width: 400px;" name="_mdrs_honey_ideas_description" id="_mdrs_honey_ideas_description" value="<?php echo get_option('_mdrs_honey_ideas_description'); ?>"></textarea>
    <?php
    }, 'theme-options', 'first_section');
    register_setting('theme-options-grp', '_mdrs_honey_ideas_description');

    //

    add_settings_section(
        'footer_section',
        'Footer Options',
        function () {
            echo '';
        },
        'theme-options'
    );

    add_settings_field('_footer_marketplaces_title',  "Section's Marketplaces Title", function () {
    ?>
        <textarea style="width: 400px;" name="_mdrs_footer_marketplaces_title" id="_mdrs_footer_marketplaces_title" value="<?php echo get_option('_mdrs_footer_marketplaces_title'); ?>"></textarea>
    <?php
    }, 'theme-options', 'footer_section');
    register_setting('theme-options-grp', '_mdrs_footer_marketplaces_title');

    add_settings_field('_footer_certificates_description',  "Section's Certificates Description", function () {
    ?>
        <textarea style="width: 400px;" name="_mdrs_footer_certificates_description" id="_mdrs_footer_certificates_description" value="<?php echo get_option('_mdrs_footer_certificates_description'); ?>"></textarea>
    <?php
    }, 'theme-options', 'footer_section');
    register_setting('theme-options-grp', '_mdrs_footer_certificates_description');

    add_settings_field('_footer_socials_label',  "Footer Socials Label", function () {
    ?>
        <input type="text" style="width: 400px;" name="_mdrs_footer_socials_label" placeholder="Ex: ( Ikuti kami di : )" id="_mdrs_footer_socials_label" value="<?php echo get_option('_mdrs_footer_socials_label'); ?>" />
<?php
    }, 'theme-options', 'footer_section');
    register_setting('theme-options-grp', '_mdrs_footer_socials_label');
}

add_action('admin_init', 'theme_preferences');
