<?php

/**
 * Taxonomy: Types.
 */

$labels = [
    "name" => "Types",
    "singular_name" => "Type",
];

$args = [
    "label" => "Types",
    "labels" => $labels,
    "public" => true,
    "publicly_queryable" => true,
    "hierarchical" => true,
    "show_ui" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "query_var" => true,
    "rewrite" => ['slug' => 'honey-and-wellness/type', 'with_front' => false],
    "show_admin_column" => true,
    "show_in_rest" => true,
    "rest_base" => "article_type",
    "rest_controller_class" => "WP_REST_Terms_Controller",
    "show_in_quick_edit" => true,
];
register_taxonomy("article_type", ["post"], $args);

/**
 * Taxonomy: Product Categories.
 */

$labels = [
    "name" => "Product Categories",
    "singular_name" => "Product Categories",
];

$args = [
    "label" => "Product Categories",
    "labels" => $labels,
    "public" => true,
    "publicly_queryable" => true,
    "hierarchical" => true,
    "show_ui" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "query_var" => true,
    "rewrite" => ['slug' => 'product/category', 'with_front' => false],
    "show_admin_column" => false,
    "show_in_rest" => true,
    "rest_base" => "product_categories",
    "rest_controller_class" => "WP_REST_Terms_Controller",
    "show_in_quick_edit" => true,
];
register_taxonomy("product_categories", ["products"], $args);

/**
 * Taxonomy: Honey FAQ Category.
 */

$labels = [
    "name" => "Honey FAQ Categories",
    "singular_name" => "Honey FAQ Categories",
];

$args = [
    "label" => "Honey FAQ Categories",
    "labels" => $labels,
    "public" => true,
    "publicly_queryable" => true,
    "hierarchical" => true,
    "show_ui" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "query_var" => true,
    "rewrite" => ['slug' => 'honey-faq/category', 'with_front' => false],
    "show_admin_column" => false,
    "show_in_rest" => true,
    "rest_base" => "faq_categories",
    "rest_controller_class" => "WP_REST_Terms_Controller",
    "show_in_quick_edit" => true,
];
register_taxonomy("faq_categories", ["honey_faq"], $args);
