<?php


function madurasa_wp_admin_scripts($scripts)
{
    $scripts->add('marketplace-widget',  get_template_directory_uri() . "/static/widgets/marketplace-widget.js", array('media-widgets'));
}
add_action('wp_default_scripts', 'madurasa_wp_admin_scripts');

/**
 * include footer scripts
 **/
function enqueue_footer_scripts()
{
    wp_enqueue_style('main-css', get_template_directory_uri() . "/static/css/main.css", array(), '1.0', false);
    wp_enqueue_script('mdrs-js', get_template_directory_uri() . "/static/js/main.js", array(), '1.0', true);
}

add_action('wp_enqueue_scripts', 'enqueue_footer_scripts');

function enqueue_header_script()
{
    $gTagPath               = ABSPATH . get_template_directory_uri() . "/static/googleanalytics.js";
    $gTagUrl                = get_template_directory_uri() . "/static/googleanalytics.js";
    $gTagBuiltTime          = filemtime($gTagPath);

    wp_enqueue_script('gtag-script', "https://www.googletagmanager.com/gtag/js?id=UA-175693433-1#asyncload", '', '', true);
    wp_enqueue_script('mdrs-gtag', $gTagUrl, "gtag-script", $gTagBuiltTime);
}

add_action('wp_header', 'enqueue_header_scripts');
