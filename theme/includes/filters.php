<?php

/**
 * remove "Archive:" from get_the_archive_title()
 **/
add_filter('get_the_archive_title', function ($title) {
    if (is_post_type_archive()) {
        $title = post_type_archive_title(' ', false);
    } elseif (is_archive()) {
        $title = single_cat_title('', false);
    } elseif (is_tag()) {
        $title = single_tag_title('', false);
    } elseif (is_author()) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif (is_category()) {
        $title = single_cat_title('', false);
    }

    return $title;
});

add_filter('template_include', function ($t) {
    $GLOBALS['current_theme_template'] = basename($t);

    return $t;
}, 1000);

add_filter('rest_endpoints', function ($endpoints) {
    if (isset($endpoints['/wp/v2/users'])) {
        unset($endpoints['/wp/v2/users']);
    }
    if (isset($endpoints['/wp/v2/users/(?P<id>[\d]+)'])) {
        unset($endpoints['/wp/v2/users/(?P<id>[\d]+)']);
    }

    return $endpoints;
});

add_filter('shared_counts_display_wrap_format', function ($wrap, $location) {
    return '<div class="%2$s m-social-share__wrapper">%1$s</div>';
}, 10, 2);

function find_n_replace_social_label($type, $output, $replace = null)
{
    $keyword = sprintf('<span class="shared-counts-label">%1$s</span>', $type);
    $pos = strpos($output, $keyword);

    $label_tag = sprintf('<span class="shared-counts-label">%1$s</span>', sprintf('Share with %1$s', $type));

    if (!is_null($replace)) {
        $label_tag = sprintf('<span class="shared-counts-label">%1$s</span>', sprintf('Share with %1$s', $replace));
    }

    if ($pos !== false) {
        $output = substr_replace($output, $label_tag, $pos, strlen($keyword));
    }

    return $output;
}

add_filter('shared_counts_display', function ($services_output, $location) {
    $services_output = find_n_replace_social_label("Facebook", $services_output);
    $services_output = find_n_replace_social_label("Tweet", $services_output, "Twitter");

    return $services_output;
}, 10, 2);
