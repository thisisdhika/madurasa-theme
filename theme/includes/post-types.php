<?php

/**
 * Post Type: Products.
 */

$labels = [
    "name" => "Products",
    "singular_name" => "Products",
];

$args = [
    "label" => "Products",
    "labels" => $labels,
    "description" => "Products",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => "products",
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "delete_with_user" => false,
    'exclude_from_search' => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => ["slug" => "products"],
    "query_var" => true,
    "menu_icon" => "dashicons-cart",
    "supports" => ["title", "editor", "thumbnail", "excerpt", "custom-fields", "revisions", "author", "page-attributes", "post-formats"],
];

register_post_type("products", $args);

/**
 * Post Type: Honey Ideas.
 */

$labels = [
    "name" => "Honey Ideas",
    "singular_name" => "Honey Ideas",
];

$args = [
    "label" => "Honey Ideas",
    "labels" => $labels,
    "description" => get_option('_mdrs_honey_ideas_description') ?: "Ada banyak resep penggunaan madu yang sehat untuk memuaskan selera Anda. Dapatkan rekomendasi resep kami di sini.",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => "honey-ideas",
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "delete_with_user" => false,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => ["slug" => "honey-ideas", "with_front" => true],
    "query_var" => true,
    "menu_icon" => "dashicons-smiley",
    "supports" => ["title", "editor", "thumbnail", "excerpt", "custom-fields", "comments", "revisions", "author", "page-attributes", "post-formats"],
];

register_post_type("honey_ideas", $args);

/**
 * Post Type: News & Events.
 */

$labels = [
    "name" => "News & Events",
    "singular_name" => "News & Events",
    "menu_name" => "News & Events",
];

$args = [
    "label" => "News & Events",
    "labels" => $labels,
    "description" => "News & Events",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => "news-and-events",
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "delete_with_user" => false,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => true,
    "rewrite" => ["slug" => "news-and-events", "with_front" => true],
    "query_var" => true,
    "menu_icon" => "dashicons-format-aside",
    "supports" => ["title", "editor", "thumbnail", "excerpt", "custom-fields", "revisions", "author", "page-attributes", "post-formats"],
];

register_post_type("news_and_events", $args);

/**
 * Post Type: Honey FAQ.
 */

$labels = [
    "name" => "Honey FAQ",
    "singular_name" => "Honey FAQ",
    "menu_name" => "Honey FAQ",
];

$args = [
    "label" => "Honey FAQ",
    "labels" => $labels,
    "description" => "Honey FAQ",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => "faqs",
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "delete_with_user" => false,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => true,
    "rewrite" => ["slug" => "faqs"],
    "query_var" => true,
    "menu_icon" => "dashicons-format-aside",
    "supports" => ["title", "editor", "thumbnail", "excerpt", "custom-fields", "revisions", "author", "page-attributes", "post-formats"],
];

register_post_type("honey_faq", $args);
