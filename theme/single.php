<?php

/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

// var_dump(get_queried_object());

$context         = Timber::context();
$timber_post     = Timber::query_post();
$context['post'] = $timber_post;
$context['is_single'] = is_single();

if ($timber_post->post_type == 'products') {
	$context['reviews'] = Timber::get_posts([
		'post_type' => 'reviews',
		"post_status"    => "publish",
		"posts_per_page" => 3,
		'meta_query' => array(
			array(
				'key' => 'target',
				'value' => $timber_post->post_name,
			),
		),
	]);
}

if (post_password_required($timber_post->ID)) {
	Timber::render('single-password.twig', $context);
} else {
	Timber::render(array('single/' . $timber_post->ID . '.twig', 'single/' . $timber_post->post_type . '.twig', 'single/' . $timber_post->slug . '.twig', 'single.twig'), $context);
}
