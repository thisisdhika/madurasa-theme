import Vue from 'vue'

import Slick from 'vue-slick'
import HomeBanner from './components/home-banner.vue'
import RecipesSlider from './components/recipes-slider.vue'
import LangSwitcher from './components/lang-switcher.vue'

Vue.component('slick', Slick)
Vue.component('home-banner', HomeBanner)
Vue.component('recipes-slider', RecipesSlider)
Vue.component('lang-switcher', LangSwitcher)

const elms = {
  'data-home-banner': {},
  'data-recipes-slider': {},
  'data-lang-switcher': {}
}

for (const selector in elms) {
  if (elms.hasOwnProperty(selector)) {
    if (document.querySelector(`[${selector}]`)) {
      const constructor = elms[selector]

      new Vue(constructor).$mount(`[${selector}]`)
    }
  }
}

document.querySelectorAll('[data-toggle]').forEach(elm => {
  const { class: cls, el: selector } = JSON.parse(elm.dataset.toggle)
  const target = document.querySelector(selector)

  elm.addEventListener('click', function() {
    target.classList.toggle(cls)
  })
})
