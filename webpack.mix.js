const mix = require('laravel-mix')

mix
  .js('src/js/main.js', 'theme/static/js')
  .sass('src/scss/main.scss', 'theme/static/css')
  .copyDirectory('src/assets', 'theme/static/assets')
  .options({
    processCssUrls: false,
    postCss: [
      require('tailwindcss')('./tailwind.config.js'),
      require('autoprefixer')
      // require("@fullhuman/postcss-purgecss")({
      //   content: ["**/*.twig"],
      //   defaultExtractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
      // }),
    ]
  })
  .webpackConfig({
    resolve: {
      alias: {
        '@src': path.resolve(__dirname, 'src'),
        '@assets': path.resolve(__dirname, 'src/assets')
      }
    }
  })
